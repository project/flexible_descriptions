<?php

namespace Drupal\flexible_descriptions;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;

/**
 * Provides common methods to work with description entity.
 */
class FlexibleDescriptionOperationsManager implements FlexibleDescriptionOperationsManagerInterface {

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Flexible descriptions helper.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionsHelper
   */
  protected FlexibleDescriptionsHelper $flexibleDescriptionsHelper;

  /**
   * Whether translation is enabled for flexible descriptions.
   *
   * @var bool
   */
  protected bool $isTranslationEnabled;

  /**
   * Constructs DescriptionEntityOperationsManager object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    FlexibleDescriptionsHelper $flexible_descriptions_helper
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->flexibleDescriptionsHelper = $flexible_descriptions_helper;
    $this->isTranslationEnabled = $this->flexibleDescriptionsHelper->isTranslationEnabled();
  }

  /**
   * {@inheritdoc}
   */
  public function createFlexibleDescription(array $field_data): bool {
    // Check if the current langcode is the langcode of a default language.
    // If not - create an original version before (if it doesn't exist)
    // and then translation.
    $flexible_description_storage = $this->entityTypeManager->getStorage('flexible_description');
    $current_langcode = $this->flexibleDescriptionsHelper->currentLangcode;
    if ($this->isTranslationEnabled) {
      $default_langcode = $this
        ->flexibleDescriptionsHelper
        ->languageManager
        ->getDefaultLanguage()
        ->getId();
      if ($default_langcode === $current_langcode) {
        // Do regular creation.
        $result = $flexible_description_storage->create($field_data)->save();
      }
      else {
        // Check if an original version exists and if not - create it with empty
        // description text + add translation.
        // Otherwise, add translation.
        $existing_description_id = $this->flexibleDescriptionsHelper->getExistingDescription($field_data['description_identifier']);
        if (!empty($existing_description_id)) {
          $existing_description_id = reset($existing_description_id);
          $existing_description = $flexible_description_storage->load($existing_description_id);
          if ($existing_description instanceof FlexibleDescriptions) {
            $untranslated = $existing_description->getUntranslated();
            if (!$untranslated->hasTranslation($current_langcode)) {
              $untranslated->addTranslation($current_langcode, $field_data);
              $result = $untranslated->save();
            }
          }
        }
        else {
          // Create original with the same description text and only then
          // add translation.
          $original_field_data = $field_data;
          $original_field_data['langcode'] = $default_langcode;
          $original = $flexible_description_storage->create($original_field_data);
          $original->addTranslation($current_langcode, $field_data);
          $result = $original->save();
        }
      }
    }
    else {
      // Do regular creation.
      $result = $flexible_description_storage->create($field_data)->save();
    }

    if (!isset($result)) {
      $result = FALSE;
    }
    return $result;
  }

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function updateFlexibleDescription(FlexibleDescriptions $flexible_description, string $description_text): bool {
    $current_langcode = $this->flexibleDescriptionsHelper->currentLangcode;
    if ($this->isTranslationEnabled) {
      if ($flexible_description->hasTranslation($current_langcode)) {
        $flexible_description = $flexible_description->getTranslation($current_langcode);
      }
      else {
        // Create a new one otherwise.
        $original_label = $flexible_description->get('label')->getString();
        $flexible_description = $flexible_description->addTranslation($current_langcode);
        $flexible_description->set('label', $original_label);
      }
    }
    $flexible_description->set('description_text', $description_text);
    return $flexible_description->save();
  }

  /**
   * {@inheritDoc}
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function deleteFlexibleDescription(FlexibleDescriptions $flexible_description): void {
    $current_langcode = $this->flexibleDescriptionsHelper->currentLangcode;
    if ($current_langcode === $this->flexibleDescriptionsHelper->languageManager->getDefaultLanguage()->getId()) {
      $this->entityTypeManager->getStorage('flexible_description')
        ->delete([$flexible_description]);
    }
    elseif ($flexible_description->hasTranslation($current_langcode)) {
      $flexible_description->removeTranslation($current_langcode);
      $flexible_description->save();
    }
  }

}
