<?php

namespace Drupal\flexible_descriptions;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeBundleInfo;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flexible_descriptions\Form\FlexibleDescriptionsSettingsForm;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides dynamic permissions for granting descriptions management access.
 *
 * @see flexible_descriptions.permissions.yml
 */
class FlexibleDescriptionsPermissions implements ContainerInjectionInterface {

  use StringTranslationTrait;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfo
   */
  protected EntityTypeBundleInfo $entityTypeBundleInfo;

  /**
   * Flexible descriptions helper service.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionsHelper
   */
  protected FlexibleDescriptionsHelper $flexibleDescriptionsHelper;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Constructs FlexibleDescriptionsPermissions object.
   */
  public function __construct(
    EntityTypeBundleInfo $entity_type_bundle_info,
    FlexibleDescriptionsHelper $flexible_descriptions_helper,
    ConfigFactoryInterface $config_factory
  ) {
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->flexibleDescriptionsHelper = $flexible_descriptions_helper;
    $this->configFactory = $config_factory;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): FlexibleDescriptionsPermissions {
    return new static(
      $container->get('entity_type.bundle.info'),
      $container->get('flexible_descriptions.helper'),
      $container->get('config.factory')
    );
  }

  /**
   * Get permissions for entity types bundles.
   */
  public function getPermissions(): array {
    $entity_types = $this->flexibleDescriptionsHelper->getSupportedEntityTypes();
    // Prepare permissions.
    $permissions = [];
    foreach ($entity_types as $entity_type) {
      $permissions[] = $this->buildPermissions($entity_type);
    }
    return array_merge([], ...$permissions);
  }

  /**
   * Build a list of permissions per entity types bundles.
   */
  public function buildPermissions(EntityTypeInterface $entity_type): array {
    $id = $entity_type->id();
    $permissions = [];
    $args['%entity_type'] = $entity_type->getLabel();
    $entity_types_configured = $this->configFactory->get(FlexibleDescriptionsSettingsForm::CONFIG_NAME)->get('entity_types');
    $bundles_ids = $entity_types_configured[$id]['bundles'];
    foreach ($this->entityTypeBundleInfo->getBundleInfo($id) as $bundle_id => $bundle_info) {
      if (\in_array($bundle_id, $bundles_ids, TRUE)) {
        $args['%entity_bundle'] = $bundle_info['label'];
        $permissions["manage flexible descriptions in {$id}|{$bundle_id}"] = ['title' => $this->t('%entity_type | %entity_bundle: Manage flexible descriptions', $args)];
      }
    }

    return $permissions;
  }

}
