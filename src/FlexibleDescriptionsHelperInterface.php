<?php

namespace Drupal\flexible_descriptions;

use Drupal\Component\Render\FormattableMarkup;

/**
 * An interface for flexible descriptions helper service.
 */
interface FlexibleDescriptionsHelperInterface {

  /**
   * Gets a list of supported entity types.
   *
   * @return \Drupal\Core\Entity\EntityTypeInterface[]
   *   Array of entity types.
   */
  public function getSupportedEntityTypes(): array;

  /**
   * Generates and returns description identifier.
   *
   * @param string $entity_type_id
   *   ID of the entity type.
   * @param string $bundle_id
   *   ID of the bundle.
   * @param string $field_name
   *   Name of the field.
   *
   * @return string
   *   Description identifier string.
   */
  public function generateDescriptionIdentifier(
    string $entity_type_id,
    string $bundle_id,
    string $field_name): string;

  /**
   * Checks whether there is existing description and returns it.
   *
   * @param string $description_identifier
   *   Description identifier string.
   *
   * @return array|int
   *   Description ID value.
   */
  public function getExistingDescription(string $description_identifier);

  /**
   * Get an array of parent keys of the '#description' item.
   *
   * @param array $field_widget_complete_form
   *   Form widget array.
   *
   * @return null|array
   *   NULL|array of '#description' parent keys.
   */
  public function getFieldWidgetDescriptionParents(array $field_widget_complete_form): ?array;

  /**
   * Get the description text by specified identifier.
   *
   * @param string $description_identifier
   *   Description identifier string.
   *
   * @return string
   *   Returns flexible description text string.
   */
  public function getFlexibleDescriptionText(string $description_identifier): string;

  /**
   * Prepares flexible description markup.
   *
   * @param mixed $text
   *   Description text.
   * @param string $description_identifier
   *   Description identifier string.
   * @param int|string|null $description_id
   *   Description id.
   *
   * @return \Drupal\Component\Render\FormattableMarkup
   *   Formattable markup.
   */
  public function getFlexibleDescriptionMarkup($text, string $description_identifier, $description_id = NULL): FormattableMarkup;

  /**
   * Checks whether translation is enabled for flexible descriptions.
   *
   * @return bool
   *   Returns true if enabled and false otherwise.
   */
  public function isTranslationEnabled(): bool;

}
