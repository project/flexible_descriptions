<?php

namespace Drupal\flexible_descriptions\Routing;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\Routing\AdminHtmlRouteProvider;
use Symfony\Component\Routing\Route;

/**
 * Provides HTML routes for entities with administrative pages.
 */
class FlexibleDescriptionsHtmlRouteProvider extends AdminHtmlRouteProvider {

  /**
   * {@inheritdoc}
   */
  protected function getCanonicalRoute(EntityTypeInterface $entity_type): ?Route {
    $route = $this->getEditFormRoute($entity_type);
    if ($route instanceof Route) {
      $route->setRequirement('_permission', 'administer flexible_description');
    }
    return $route;
  }

}
