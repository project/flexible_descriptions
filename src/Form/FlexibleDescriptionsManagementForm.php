<?php

namespace Drupal\flexible_descriptions\Form;

use Drupal\Core\Entity\EntityDisplayRepositoryInterface;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Language\LanguageInterface;
use Drupal\Core\Render\Element;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;
use Drupal\flexible_descriptions\FlexibleDescriptionOperationsManagerInterface;
use Drupal\flexible_descriptions\FlexibleDescriptionsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a form that allows to perform CRUD on flexible descriptions.
 */
class FlexibleDescriptionsManagementForm extends FormBase {

  /**
   * EntityTypeManager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * EntityDisplayRepository service.
   *
   * @var \Drupal\Core\Entity\EntityDisplayRepositoryInterface
   */
  protected EntityDisplayRepositoryInterface $entityDisplayRepository;

  /**
   * EntityFieldManager service.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected EntityFieldManagerInterface $entityFieldManager;

  /**
   * Flexible description helper service.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionsHelperInterface
   */
  protected FlexibleDescriptionsHelperInterface $flexibleDescriptionHelper;

  /**
   * Flexible description operations manager service.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionOperationsManagerInterface
   */
  protected FlexibleDescriptionOperationsManagerInterface $flexibleDescriptionOperationsManager;

  /**
   * Current user service.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected AccountProxyInterface $currentUser;

  /**
   * Current lang.
   *
   * @var \Drupal\Core\Language\LanguageInterface
   */
  protected LanguageInterface $currentLang;

  /**
   * Constructs DescriptionsMappingForm object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    EntityDisplayRepositoryInterface $entity_display_repository,
    EntityFieldManagerInterface $entity_field_manager,
    FlexibleDescriptionsHelperInterface $flexible_description_helper,
    FlexibleDescriptionOperationsManagerInterface $flexible_description_operations_manager,
    AccountProxyInterface $account_proxy
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->entityDisplayRepository = $entity_display_repository;
    $this->entityFieldManager = $entity_field_manager;
    $this->flexibleDescriptionHelper = $flexible_description_helper;
    $this->flexibleDescriptionOperationsManager = $flexible_description_operations_manager;
    $this->currentUser = $account_proxy;
    $this->currentLang = $this->flexibleDescriptionHelper->languageManager->getCurrentLanguage();
  }

  /**
   * Creates DescriptionsMappingForm instance.
   */
  public static function create(ContainerInterface $container): FlexibleDescriptionsManagementForm {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('entity_display.repository'),
      $container->get('entity_field.manager'),
      $container->get('flexible_descriptions.helper'),
      $container->get('flexible_descriptions.operations_manager'),
      $container->get('current_user')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'flexible_descriptions_management_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    // Loop only through supported entity types.
    $supported_entity_types = $this->flexibleDescriptionHelper->getSupportedEntityTypes();
    $config = $this->configFactory()->get(FlexibleDescriptionsSettingsForm::CONFIG_NAME)->get();
    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $supported_entity_type */
    foreach ($supported_entity_types as $supported_entity_type) {
      $entity_type_id = $supported_entity_type->id();
      $bundle_entity_type = $supported_entity_type->getBundleEntityType() ?? $entity_type_id;
      $entity_type_label = $supported_entity_type->getLabel();
      $entity_bundles = $this->entityTypeManager->getStorage($bundle_entity_type)
        ->loadMultiple($config['entity_types'][$supported_entity_type->id()]['bundles']);
      foreach ($entity_bundles as $entity_bundle) {
        $entity_bundle_id = $entity_bundle->id();
        // Set the container with #tree, so it's possible to separate the data
        // by entity type id.
        if (!isset($form[$entity_type_id])) {
          $form[$entity_type_id] = [
            '#type' => 'details',
            '#tree' => TRUE,
            '#title' => $entity_type_label,
            '#attributes' => [
              'class' => ['entity-type-level'],
            ],
          ];
        }
        // Check access before building remaining parts.
        $access = $this->currentUser->hasPermission("manage flexible descriptions in {$entity_type_id}|{$entity_bundle_id}");
        if (!$access) {
          continue;
        }
        $form[$entity_type_id][$entity_bundle_id] = [
          '#type' => 'details',
          '#title' => $entity_bundle->label(),
          '#attributes' => [
            'class' => ['entity-bundle-level'],
          ],
        ];

        $form[$entity_type_id][$entity_bundle_id]['entity_bundle_fields_descriptions'] = [
          '#type' => 'details',
          '#title' => $this->t('Flexible descriptions'),
          '#attributes' => [
            'class' => ['fields-descriptions-level'],
          ],
        ];
        $field_definitions = $this->entityFieldManager->getFieldDefinitions($entity_bundle->getEntityType()->getBundleOf(), $entity_bundle_id);

        // Skip fields that have no form display_context.
        $field_definitions_filtered = array_filter($field_definitions, static function ($field_definition) {
          return !empty($field_definition->getDisplayOptions('form'));
        });
        foreach (array_keys($field_definitions_filtered) as $field_name) {
          $description_identifier = $this->flexibleDescriptionHelper->generateDescriptionIdentifier($entity_type_id, $entity_bundle_id, $field_name);
          $form[$entity_type_id][$entity_bundle_id]['entity_bundle_fields_descriptions'][$field_name] = [
            '#type' => 'textarea',
            '#title' => $field_definitions_filtered[$field_name]->getLabel(),
            '#default_value' => $this->flexibleDescriptionHelper->getFlexibleDescriptionText($description_identifier),
          ];
        }
      }

      // At the end of a loop iteration - check whether there is at least one
      // bundle inside entity_type container. If no - just unset it, no need
      // to build empty details element.
      if (!empty($form[$entity_type_id]) && empty(Element::children($form[$entity_type_id]))) {
        unset($form[$entity_type_id]);
      }
    }
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#name' => 'save',
      '#value' => $this->t('Save'),
      '#button_type' => 'primary',
    ];
    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $current_language_is_default = $this->currentLang->isDefault();
    $description_storage = $this->entityTypeManager->getStorage('flexible_description');
    foreach ($form_state->cleanValues()->getValues() as $entity_type_id => $array_values) {
      foreach ($array_values as $bundle_id => $values) {
        foreach ($values as $value) {
          // In this case we got an associative array where the key is field
          // name and value is its description. So, loop through them.
          foreach ($value as $field_name => $field_description) {
            $description_identifier = $this->flexibleDescriptionHelper->generateDescriptionIdentifier($entity_type_id, $bundle_id, $field_name);
            $existing_description_id = $this->flexibleDescriptionHelper->getExistingDescription($description_identifier);
            $description_entity_data = [
              'label' => 'Flexible description | ' . $description_identifier,
              'description_identifier' => $description_identifier,
              'description_text' => $field_description,
            ];
            // Possible cases:
            // 1. We have existing entity id.
            // 1.2. We're on default language context.
            // 1.3. We're on non-default language context.
            // 2. We don't have existing entity id.
            // 2.2 We're on default language context.
            // 2.3 We're on non-default language context.
            // What to do:
            // 1.2 If empty description - remove entity. Update original
            // otherwise.
            // 1.3 If empty description - remove translation entity. Update
            // translation otherwise.
            // 2.2 If empty description - do nothing.
            // Create a description original version otherwise.
            // 2.3 If empty description - do nothing. Create an original and
            // translation version with the same description text.
            if (!empty($existing_description_id)) {
              $existing_description = $description_storage->load(reset($existing_description_id));
              if (!$existing_description instanceof FlexibleDescriptions) {
                throw new \RuntimeException('Failed to load flexible description.');
              }
              if (empty($field_description)) {
                $this->flexibleDescriptionOperationsManager->deleteFlexibleDescription($existing_description);
              }
              else {
                $this->flexibleDescriptionOperationsManager->updateFlexibleDescription($existing_description, $field_description);
              }
            }
            elseif (!empty($field_description)) {
              $this->flexibleDescriptionOperationsManager->createFlexibleDescription($description_entity_data);
            }
          }
        }
      }
    }

    $this->messenger()->addStatus($this->t('Processed successfully.'));
  }

}
