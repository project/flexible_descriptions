<?php

namespace Drupal\flexible_descriptions\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\ContentEntityTypeInterface;
use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form for a flexible descriptions entity type.
 */
class FlexibleDescriptionsSettingsForm extends ConfigFormBase {

  /**
   * Config name string.
   */
  public const CONFIG_NAME = 'flexible_descriptions.settings';

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Constructs a FlexibleDescriptionsSettingsForm object.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    EntityTypeBundleInfoInterface $entity_type_bundle_info
  ) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
  }

  /**
   * Creates a FlexibleDescriptionsSettingsForm instance.
   */
  public static function create(ContainerInterface $container): FlexibleDescriptionsSettingsForm {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager'),
      $container->get('entity_type.bundle.info')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'flexible_description_settings';
  }

  /**
   * {@inheritDoc}
   */
  public function getEditableConfigNames(): array {
    return [self::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $entity_types = $this->entityTypeManager->getDefinitions();
    // Allow to configure only content entity types and exclude flexible
    // descriptions entity type.
    $content_entity_types = array_filter($entity_types, static function ($entity_type) {
      return $entity_type instanceof ContentEntityTypeInterface && $entity_type->id() !== 'flexible_description';
    });
    $config = $this->config(self::CONFIG_NAME)->get();

    /** @var \Drupal\Core\Entity\ContentEntityTypeInterface $content_entity_type */
    foreach ($content_entity_types as $content_entity_type) {
      $entity_type_id = $content_entity_type->id();
      $form[$entity_type_id] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];
      $form[$entity_type_id]['is_enabled'] = [
        '#type' => 'checkbox',
        '#title' => $content_entity_type->getLabel(),
        '#description' => $this->t('Enable flexible descriptions for this entity type?'),
        '#default_value' => $config['entity_types'][$entity_type_id]['is_supported'] ?? FALSE,
      ];
      $form[$entity_type_id]['enabled_bundles'] = [
        '#type' => 'details',
        '#title' => $this->t('Enabled bundles'),
        '#description' => $this->t('Leave empty to enable for all the bundles'),
        '#open' => TRUE,
        '#states' => [
          'visible' => [
            ":input[name='{$entity_type_id}[is_enabled]']" => ['checked' => TRUE],
          ],
        ],
      ];
      $bundles_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      foreach ($bundles_info as $bundle_id => $bundle_info) {
        $form[$entity_type_id]['enabled_bundles'][$bundle_id] = [
          '#type' => 'checkbox',
          '#title' => $bundle_info['label'],
          '#description' => $this->t('Enable flexible descriptions for this bundle?'),
          '#default_value' => !empty($config['entity_types'][$entity_type_id]['bundles']) && \in_array($bundle_id, $config['entity_types'][$entity_type_id]['bundles'], TRUE),
        ];
      }
    }
    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $clean_values = $form_state->cleanValues()->getValues();
    $config_data = [];
    foreach ($clean_values as $entity_type_id => $entity_type_info) {
      if (!$entity_type_info['is_enabled']) {
        $config_data[$entity_type_id] = [
          'is_supported' => FALSE,
          'bundles' => [],
        ];
        continue;
      }
      // Get supported bundles list, depending on checkboxes values.
      // If entity_type is enabled but no bundles selected - include all the
      // bundles.
      if (empty($enabled_bundles = array_filter($entity_type_info['enabled_bundles']))) {
        $supported_bundles = array_keys($entity_type_info['enabled_bundles']);
      }
      else {
        $supported_bundles = array_keys($enabled_bundles);
      }
      $config_data[$entity_type_id] = [
        'is_supported' => TRUE,
        'bundles' => $supported_bundles,
      ];
    }
    $this->config(self::CONFIG_NAME)->setData(['entity_types' => $config_data])->save();
    parent::submitForm($form, $form_state);
  }

}
