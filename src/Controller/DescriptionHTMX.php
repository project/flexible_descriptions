<?php

namespace Drupal\flexible_descriptions\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Access\AccessResultInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Render\RendererInterface;
use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;
use Drupal\flexible_descriptions\FlexibleDescriptionOperationsManagerInterface;
use Drupal\flexible_descriptions\FlexibleDescriptionsHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns form that allows to add flexible description.
 */
class DescriptionHTMX extends ControllerBase {

  /**
   * Request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * Renderer.
   *
   * @var \Drupal\Core\Render\RendererInterface
   */
  protected RendererInterface $renderer;

  /**
   * Flexible descriptions helper.
   */
  protected FlexibleDescriptionsHelperInterface $flexibleDescriptionsHelper;

  /**
   * Flexible descriptions operations manager.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionOperationsManagerInterface
   */
  protected FlexibleDescriptionOperationsManagerInterface $flexibleDescriptionsOperationsManager;

  /**
   * Constructs DescriptionHTMX object.
   */
  public function __construct(
    RequestStack $request_stack,
    RendererInterface $renderer,
    FlexibleDescriptionsHelperInterface $flexible_descriptions_helper,
    FlexibleDescriptionOperationsManagerInterface $flexible_descriptions_operations_manager
  ) {
    $this->requestStack = $request_stack;
    $this->renderer = $renderer;
    $this->flexibleDescriptionsHelper = $flexible_descriptions_helper;
    $this->flexibleDescriptionsOperationsManager = $flexible_descriptions_operations_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): DescriptionHTMX {
    return new static(
      $container->get('request_stack'),
      $container->get('renderer'),
      $container->get('flexible_descriptions.helper'),
      $container->get('flexible_descriptions.operations_manager')
    );
  }

  /**
   * Builds the response.
   */
  public function build(string $description_identifier, string $description_id): Response {
    // What we need here is just a form with one field.
    if (!empty($description_id)) {
      $description_text = $this->flexibleDescriptionsHelper->getFlexibleDescriptionText($description_identifier);
    }
    $current_lang = $this->languageManager()->getCurrentLanguage();
    $array_to_render = [
      '#theme' => 'flexible_descriptions_htmx',
      '#description_identifier' => $description_identifier,
      '#description_text' => $description_text ?? '',
      '#description_id' => $description_id,
      '#current_langcode' => $current_lang->isDefault() ? '' : $current_lang->getId(),
    ];
    $rendered = $this->renderer->renderRoot($array_to_render);
    return new Response($rendered);
  }

  /**
   * Adds description entity.
   */
  public function performAction(): Response {
    $request = $this->requestStack->getCurrentRequest()->request;
    $request_data = [
      'description-identifier' => $request->get('description-identifier'),
      'description-text' => $request->get('description-text'),
      'description-id' => $request->get('description-id'),
    ];
    // Try to search id in the database as well.
    // This is for the case when we have the original version and adding
    // translation.
    if (empty($request_data['description-id'])) {
      $id_from_db = $this->flexibleDescriptionsHelper->getExistingDescription($request_data['description-identifier']);
      if (!empty($id_from_db)) {
        $request_data['description-id'] = reset($id_from_db);
      }
    }
    // Add/update description in the database.
    // Return description back.
    $result = 0;
    $description_storage = $this->entityTypeManager()->getStorage('flexible_description');
    if (!empty($request_data['description-id'])) {
      $existing_description = $description_storage->load($request_data['description-id']);
      if (!$existing_description instanceof FlexibleDescriptions) {
        throw new \RuntimeException('Failed to load flexible description.');
      }
      if (empty($request_data['description-text'])) {
        $this->flexibleDescriptionsOperationsManager->deleteFlexibleDescription($existing_description);
        $request_data['description-id'] = NULL;
      }
      else {
        $result = $this->flexibleDescriptionsOperationsManager->updateFlexibleDescription($existing_description, $request_data['description-text']);
      }
    }
    elseif (!empty($request_data['description-text'])) {
      $result = $this->flexibleDescriptionsOperationsManager->createFlexibleDescription([
        'label' => 'Flexible description | ' . $request_data['description-identifier'],
        'description_text' => $request_data['description-text'],
        'description_identifier' => $request_data['description-identifier'],
      ]);
      $description_id = $this->flexibleDescriptionsHelper->getExistingDescription($request_data['description-identifier']);
    }
    $markup = $this->flexibleDescriptionsHelper->getFlexibleDescriptionMarkup($request_data['description-text'], $request_data['description-identifier'], isset($description_id) ? reset($description_id) : $request_data['description-id']);
    if ($result === SAVED_NEW || $result === SAVED_UPDATED) {
      $description_id = isset($description_id) ? reset($description_id) : NULL;
      $markup = $this->flexibleDescriptionsHelper->getFlexibleDescriptionMarkup($request_data['description-text'], $request_data['description-identifier'], isset($existing_description) ? $existing_description->id() : $description_id);
    }
    return new Response((string) $markup);

  }

  /**
   * Returns cancelled result.
   */
  public function getCancelledResult(string $description_identifier, string $description_id = NULL): Response {
    $description_text = $this->flexibleDescriptionsHelper->getFlexibleDescriptionText($description_identifier);
    $markup = $this->flexibleDescriptionsHelper->getFlexibleDescriptionMarkup($description_text, $description_identifier, $description_id);
    return new Response((string) $markup);
  }

  /**
   * Checks access for edit/cancel operations.
   */
  public function checkAccess($description_identifier = NULL): AccessResultInterface {
    // In case it's a post request - identifier is null, so try to get it from
    // the post data.
    if (!$description_identifier) {
      $description_identifier = $this->requestStack->getCurrentRequest()->request->get('description-identifier');
    }
    if ($description_identifier) {
      [$entity_type_id, $bundle] = explode('|', $description_identifier);
      return AccessResult::allowedIf($this->currentUser()
        ->hasPermission("manage flexible descriptions in {$entity_type_id}|{$bundle}"));
    }
    return AccessResult::forbidden('Please set a proper permissions to be able to create/update flexible descriptions.');
  }

}
