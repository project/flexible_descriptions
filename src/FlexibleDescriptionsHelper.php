<?php

namespace Drupal\flexible_descriptions;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Url;
use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;
use Drupal\flexible_descriptions\Form\FlexibleDescriptionsSettingsForm;

/**
 * Provides a helper service with useful methods for flexible descriptions.
 */
class FlexibleDescriptionsHelper implements FlexibleDescriptionsHelperInterface {

  /**
   * The Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * Module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Language manager.
   *
   * @var \Drupal\Core\Language\LanguageManagerInterface
   */
  public LanguageManagerInterface $languageManager;

  /**
   * Current langcode.
   *
   * @var string
   */
  public string $currentLangcode;

  /**
   * Constructs FlexibleDescriptionsHelper object.
   */
  public function __construct(
    EntityTypeManagerInterface $entity_type_manager,
    ConfigFactoryInterface $config_factory,
    ModuleHandlerInterface $module_handler,
    LanguageManagerInterface $language_manager
  ) {
    $this->entityTypeManager = $entity_type_manager;
    $this->configFactory = $config_factory;
    $this->moduleHandler = $module_handler;
    $this->languageManager = $language_manager;
    $this->currentLangcode = $this->languageManager->getCurrentLanguage()->getId();
  }

  /**
   * Returns an array of supported entity types.
   */
  public function getSupportedEntityTypes(): array {
    $entity_types_info = $this->configFactory->get(FlexibleDescriptionsSettingsForm::CONFIG_NAME)->get('entity_types');
    $entity_types = [];
    foreach ($entity_types_info as $entity_type_id => $entity_type_info) {
      if ($entity_type_info['is_supported']) {
        $entity_types[] = $this->entityTypeManager->getDefinition($entity_type_id);
      }
    }

    return !empty($entity_types) ? $entity_types : [];
  }

  /**
   * {@inheritdoc}
   */
  public function generateDescriptionIdentifier(
    string $entity_type_id,
    string $bundle_id,
    string $field_name
  ): string {
    return implode('|', [
      $entity_type_id,
      $bundle_id,
      $field_name,
    ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getExistingDescription(string $description_identifier) {
    return $this->entityTypeManager->getStorage('flexible_description')
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('description_identifier', $description_identifier)
      ->execute();
  }

  /**
   * {@inheritdoc}
   */
  public function getFieldWidgetDescriptionParents(array $field_widget_complete_form): ?array {
    foreach ($field_widget_complete_form as $key => $value) {
      if ($key === '#description') {
        return [$key];
      }
      if (is_array($value)) {
        $nested_path = $this->getFieldWidgetDescriptionParents($value);
        if ($nested_path !== NULL) {
          array_unshift($nested_path, $key);
          return $nested_path;
        }
      }
    }
    return NULL;
  }

  /**
   * Checks whether translation is enabled for flexible descriptions.
   */
  public function isTranslationEnabled(): bool {
    $enabled = FALSE;
    if ($this->moduleHandler->moduleExists('content_translation')) {
      // We cannot inject content translation manager service cuz content
      // translation module could be disabled.
      // @phpstan-ignore-next-line
      $enabled = \Drupal::service('content_translation.manager')->isEnabled('flexible_description');
    }

    return $enabled;
  }

  /**
   * {@inheritdoc}
   */
  public function getFlexibleDescriptionText(string $description_identifier) : string {
    $description_text = '';
    $flexible_description_storage = $this->entityTypeManager->getStorage('flexible_description');
    $flexible_description_id = $flexible_description_storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('description_identifier', $description_identifier)
      ->execute();
    if (!empty($flexible_description_id) && is_array($flexible_description_id)) {
      $flexible_description = $flexible_description_storage->load(reset($flexible_description_id));
      if ($flexible_description instanceof FlexibleDescriptions) {
        // If translations are enabled - return description from translated
        // entity if it exists or empty string otherwise.
        if ($this->isTranslationEnabled()) {
          if ($flexible_description->hasTranslation($this->currentLangcode)) {
            $description_text = $flexible_description->getTranslation($this->currentLangcode)->get('description_text')->getString();
          }
        }
        else {
          $description_text = $flexible_description->get('description_text')
            ->getString();
        }
      }
    }

    return $description_text;
  }

  /**
   * {@inheritDoc}
   */
  public function getFlexibleDescriptionMarkup($text, string $description_identifier, $description_id = NULL): FormattableMarkup {
    $endpoint_url = Url::fromRoute('flexible_descriptions.htmx_form', [
      'description_identifier' => $description_identifier,
      'description_id' => $description_id,
    ]);
    $endpoint_url_string = $endpoint_url->access() ? $endpoint_url->toString() : '';
    $button_markup = '';
    if (!empty($endpoint_url_string)) {
      $button_markup = new FormattableMarkup('
        <button id="@id" hx-get=":endpoint" class="btn flexible-description-action button">
          @button_label
        </button>', [
          ':endpoint' => $endpoint_url_string,
          '@button_label' => $description_id && !empty($text) ? (string) t('Edit description') : (string) t('Add flexible description'),
          '@id' => $description_identifier,
        ]);
    }
    return new FormattableMarkup('
    <div class="flexible-description-wrapper" hx-target="this" hx-swap="outerHTML">
      <div class="flexible-description-text">@text</div>
      @button_markup
    </div>',
      [
        '@text' => $text,
        '@button_markup' => $button_markup,
      ]);
  }

}
