<?php

namespace Drupal\flexible_descriptions;

use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;

/**
 * Provides an interface for flexible descriptions operations manager.
 */
interface FlexibleDescriptionOperationsManagerInterface {

  /**
   * Creates flexible description entity by provided field data.
   *
   * @param array $field_data
   *   Field data array.
   */
  public function createFlexibleDescription(array $field_data): bool;

  /**
   * Updates flexible description entity.
   *
   * @param \Drupal\flexible_descriptions\Entity\FlexibleDescriptions $flexible_description
   *   Flexible description entity.
   * @param string $description_text
   *   Updated description text.
   */
  public function updateFlexibleDescription(FlexibleDescriptions $flexible_description, string $description_text): bool;

  /**
   * Deletes flexible description entity.
   *
   * @param \Drupal\flexible_descriptions\Entity\FlexibleDescriptions $flexible_description
   *   Flexible description entity.
   */
  public function deleteFlexibleDescription(FlexibleDescriptions $flexible_description): void;

}
