<?php

namespace Drupal\flexible_descriptions;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the flexible descriptions entity type.
 *
 * @see \Drupal\flexible_descriptions\Entity\FlexibleDescriptions
 * @ingroup flexible_descriptions_access
 */
class FlexibleDescriptionsAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  public function access(EntityInterface $entity, $operation, AccountInterface $account = NULL, $return_as_object = FALSE) {
    $account = $this->prepareUser($account);

    // Bypass if the user has administration permission.
    if ($account->hasPermission('administer flexible_description')) {
      $result = AccessResult::allowed()->cachePerPermissions();
      return $return_as_object ? $result : $result->isAllowed();
    }
    if (!$account->hasPermission('manage flexible descriptions')) {
      $result = AccessResult::forbidden("The 'manage flexible descriptions' permission is required.")->cachePerPermissions();
      return $return_as_object ? $result : $result->isAllowed();
    }
    $entity_type_id = $entity->getEntityTypeId();
    $entity_bundle = $entity->bundle();
    if ($account->hasPermission("manage flexible descriptions in {$entity_type_id}|{$entity_bundle}")) {
      $result = AccessResult::allowed()->cachePerPermissions();
      return $return_as_object ? $result : $result->isAllowed();
    }
    $result = parent::access($entity, $operation, $account, TRUE)->cachePerPermissions();

    return $return_as_object ? $result : $result->isAllowed();
  }

}
