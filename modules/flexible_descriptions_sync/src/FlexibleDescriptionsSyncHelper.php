<?php

namespace Drupal\flexible_descriptions_sync;

use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Provides useful methods for the export of flexible descriptions.
 */
class FlexibleDescriptionsSyncHelper implements FlexibleDescriptionsSyncHelperInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs FlexibleDescriptionsSyncHelper object.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * Returns an array of flexible descriptions, found by entity_type, bundles.
   */
  public function getAllDescriptionsOfBundles(string $entity_type_id, array $bundles): array {
    $entities = [];
    foreach ($bundles as $bundle) {
      $entities[] = $this->getAllDescriptionsOfBundle($entity_type_id, $bundle);
    }

    // Remove empty entries if any.
    return array_merge([], ...array_filter($entities));
  }

  /**
   * Returns an array of flexible descriptions, found by entity_type and bundle.
   */
  public function getAllDescriptionsOfBundle(string $entity_type_id, string $bundle): array {
    $flexible_descriptions_storage = $this->entityTypeManager->getStorage('flexible_description');
    $ids = $flexible_descriptions_storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('description_identifier', $entity_type_id . '|' . $bundle, 'STARTS_WITH')
      ->execute();
    if ($ids) {
      return $flexible_descriptions_storage->loadMultiple($ids);
    }

    return [];
  }

}
