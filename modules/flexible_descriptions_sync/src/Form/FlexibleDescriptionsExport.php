<?php

namespace Drupal\flexible_descriptions_sync\Form;

use Drupal\Core\Entity\EntityTypeBundleInfoInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\Element;
use Drupal\flexible_descriptions\FlexibleDescriptionsHelper;
use Drupal\flexible_descriptions\Form\FlexibleDescriptionsSettingsForm;
use Drupal\flexible_descriptions_sync\FlexibleDescriptionsSyncHelper;
use Drupal\single_content_sync\ContentFileGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a form to export flexible descriptions.
 *
 * @package Drupal\flexible_descriptions_sync\Form
 */
class FlexibleDescriptionsExport extends FormBase {

  /**
   * Flexible descriptions helper service.
   *
   * @var \Drupal\flexible_descriptions\FlexibleDescriptionsHelper
   */
  protected FlexibleDescriptionsHelper $flexibleDescriptionsHelper;

  /**
   * Entity type bundle info service.
   *
   * @var \Drupal\Core\Entity\EntityTypeBundleInfoInterface
   */
  protected EntityTypeBundleInfoInterface $entityTypeBundleInfo;

  /**
   * Flexible descriptions sync helper service.
   *
   * @var \Drupal\flexible_descriptions_sync\FlexibleDescriptionsSyncHelper
   */
  protected FlexibleDescriptionsSyncHelper $flexibleDescriptionsSyncHelper;

  /**
   * Content file generator service.
   *
   * @var \Drupal\single_content_sync\ContentFileGeneratorInterface
   */
  protected ContentFileGeneratorInterface $contentFileGenerator;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Constructs FlexibleDescriptionsExport object.
   */
  public function __construct(
    FlexibleDescriptionsHelper $flexible_descriptions_helper,
    EntityTypeBundleInfoInterface $entity_type_bundle_info,
    FlexibleDescriptionsSyncHelper $flexible_descriptions_sync_helper,
    ContentFileGeneratorInterface $content_file_generator,
    EntityTypeManagerInterface $entity_type_manager
  ) {
    $this->flexibleDescriptionsHelper = $flexible_descriptions_helper;
    $this->entityTypeBundleInfo = $entity_type_bundle_info;
    $this->flexibleDescriptionsSyncHelper = $flexible_descriptions_sync_helper;
    $this->contentFileGenerator = $content_file_generator;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): FlexibleDescriptionsExport {
    return new static(
      $container->get('flexible_descriptions.helper'),
      $container->get('entity_type.bundle.info'),
      $container->get('flexible_descriptions_sync.helper'),
      $container->get('single_content_sync.file_generator'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritDoc}
   */
  public function getFormId(): string {
    return 'flexible_descriptions_sync_export_form';
  }

  /**
   * {@inheritDoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $supported_entity_types = $this->flexibleDescriptionsHelper->getSupportedEntityTypes();
    $config = $this->configFactory()->get(FlexibleDescriptionsSettingsForm::CONFIG_NAME)->get('entity_types');
    // Start from building a container per entity_type.
    /** @var \Drupal\Core\Entity\EntityTypeInterface $entity_type */
    foreach ($supported_entity_types as $entity_type) {
      $entity_type_id = $entity_type->id();
      $entity_type_label = $entity_type->getLabel();
      $supported_bundles = $config[$entity_type_id]['bundles'];
      $form[$entity_type_id] = [
        '#type' => 'container',
        '#tree' => TRUE,
      ];

      // Allow to enable/disable entity_types.
      $form[$entity_type_id]['enable_entity_type'] = [
        '#type' => 'checkbox',
        '#title' => $entity_type_label,
        '#description' => $this->t('Include %entity_type_label flexible descriptions in export? If enabled and no bundles selected - flexible descriptions of all the bundles will be exported.', [
          '%entity_type_label' => $entity_type_label,
        ]),
      ];

      // Prepare the details element and checkboxes per bundles.
      $form[$entity_type_id]['enabled_bundles'] = [
        '#type' => 'details',
        '#open' => TRUE,
        '#title' => $this->t('Enabled bundles'),
        '#states' => [
          'visible' => [
            ":input[name='{$entity_type_id}[enable_entity_type]']" => ['checked' => TRUE],
          ],
        ],
      ];
      $bundles_info = $this->entityTypeBundleInfo->getBundleInfo($entity_type_id);
      foreach ($bundles_info as $bundle_id => $bundle_info) {
        if (\in_array($bundle_id, $supported_bundles, TRUE)) {
          $form[$entity_type_id]['enabled_bundles'][$bundle_id] = [
            '#type' => 'checkbox',
            '#title' => $bundle_info['label'],
            '#description' => $this->t('Include this bundle in export?'),
          ];
        }
      }
    }

    // Add submit action.
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Export'),
    ];

    return $form;
  }

  /**
   * {@inheritDoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Get the arrays of values, keyed by entity_type.
    $values = $form_state->cleanValues()->getValues();
    // Filter disabled entity_types.
    $values_filtered = array_filter($values, static function ($info) {
      return $info['enable_entity_type'];
    });

    if (empty($values_filtered)) {
      $this->messenger()->addWarning($this->t('No entity types selected, export failed.'));
      return;
    }
    // Loop through enabled entity_types and their bundles. Prepare the
    // info array before export.
    $export_info = [];
    foreach ($values_filtered as $entity_type_id => $entity_type_export_info) {
      // Determine which bundles are enabled. If entity_type is enabled
      // but no bundles are enabled - include all.
      if (!\in_array(1, $entity_type_export_info['enabled_bundles'], TRUE)) {
        $export_info[$entity_type_id] = Element::children($form[$entity_type_id]['enabled_bundles']);
        continue;
      }
      // And include only enabled ones if such exist.
      $export_info[$entity_type_id] = array_keys(array_filter($entity_type_export_info['enabled_bundles']));
    }

    // Now, search exportable entities.
    $entities = [];
    foreach ($export_info as $entity_type_id => $bundles) {
      $entities[] = $this->flexibleDescriptionsSyncHelper->getAllDescriptionsOfBundles($entity_type_id, $bundles);
    }
    $entities = array_merge([], ...$entities);
    if (empty($entities)) {
      $this->messenger()->addWarning($this->t('No exportable descriptions found.'));
      return;
    }
    $archive = $this->contentFileGenerator->generateBulkZipFile($entities, TRUE);
    [$file_scheme, $file_target] = explode('://', $archive->getFileUri(), 2);

    $this->messenger()->addStatus($this->t('We have successfully exported flexible descriptions. Follow the @link to download the generate zip file with the content.', [
      '@link' => Link::createFromRoute($this->t('link'), 'single_content_sync.file_download', ['scheme' => $file_scheme], [
        'query' => [
          'file' => $file_target,
        ],
        'attributes' => [
          'class' => ['export-link'],
        ],
      ])->toString(),
    ]));
  }

}
