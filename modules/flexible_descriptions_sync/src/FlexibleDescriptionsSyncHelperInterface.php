<?php

namespace Drupal\flexible_descriptions_sync;

/**
 * Creates an interface for flexible descriptions sync helper.
 */
interface FlexibleDescriptionsSyncHelperInterface {

  /**
   * Searches for the flexible description entities of bundles of entity_type.
   *
   * @param string $entity_type_id
   *   Entity type id.
   * @param array $bundles
   *   Bundles ids array.
   *
   * @return \Drupal\flexible_descriptions\Entity\FlexibleDescriptions[]
   *   Returns array of flexible descriptions.
   */
  public function getAllDescriptionsOfBundles(string $entity_type_id, array $bundles): array;

  /**
   * Searches for the flexible description entities of bundle of entity_type.
   *
   * @param string $entity_type_id
   *   Entity type id.
   * @param string $bundle
   *   Bundle id.
   *
   * @return \Drupal\flexible_descriptions\Entity\FlexibleDescriptions[]
   *   Returns array of flexible descriptions.
   */
  public function getAllDescriptionsOfBundle(string $entity_type_id, string $bundle): array;

}
