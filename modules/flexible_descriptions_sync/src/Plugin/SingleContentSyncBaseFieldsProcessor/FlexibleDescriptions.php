<?php

namespace Drupal\flexible_descriptions_sync\Plugin\SingleContentSyncBaseFieldsProcessor;

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\single_content_sync\SingleContentSyncBaseFieldsProcessorPluginBase;

/**
 * Plugin implementation for flexible descriptions base fields processor plugin.
 *
 * @SingleContentSyncBaseFieldsProcessor(
 *   id = "flexible_description",
 *   label = @Translation("Flexible descriptions fields processor"),
 *   entity_type = "flexible_description",
 * )
 */
class FlexibleDescriptions extends SingleContentSyncBaseFieldsProcessorPluginBase {

  /**
   * {@inheritdoc}
   */
  public function mapBaseFieldsValues(array $values): array {
    return [
      'label' => $values['label'],
      'langcode' => $values['langcode'],
      'description_text' => $values['description_text'],
      'description_identifier' => $values['description_identifier'],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function exportBaseValues(FieldableEntityInterface $entity): array {
    return [
      'langcode' => $entity->language()->getId(),
      'label' => $entity->get('label')->getString(),
      'description_text' => $entity->get('description_text')->getString(),
      'description_identifier' => $entity->get('description_identifier')->getString(),
    ];
  }

}
