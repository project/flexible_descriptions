<?php

namespace Drupal\Tests\flexible_descriptions_sync\Functional;

use Drupal\Core\Serialization\Yaml;
use Drupal\file\Entity\File;

/**
 * Provides tests of the flexible descriptions export functionality.
 *
 * @group flexible_descriptions_sync
 */
class ExportTest extends FlexibleDescriptionsSyncBrowserTestBase {

  /**
   * Test export functionality.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testFlexibleDescriptionsExport(): void {
    // Import description entity first.
    $result = \Drupal::entityTypeManager()->getStorage('flexible_description')->create(
      $this->exampleEntity['base_fields']
    )->save();
    $this->assertEquals(SAVED_NEW, $result);
    $this->drupalGet('admin/content/export-flexible-descriptions');
    $this->assertSession()->statusCodeEquals(200);

    // Submit form with a checked Article bundle, so it will be included in the
    // export.
    $this->submitForm([
      'node[enable_entity_type]' => 1,
      'node[enabled_bundles][article]' => 1,
    ], 'Export');
    $this->assertSession()->elementExists('xpath', "//a[contains(@class, 'export-link')]");
    $href = $this->assertSession()->elementExists('xpath', "//a[contains(@class, 'export-link')]")->getAttribute('href');
    // Search for file entity and generate download url using its uri.
    $file = \Drupal::entityTypeManager()->getStorage('file')
      ->loadByProperties(['filename' => basename($href)]);
    $this->assertNotEmpty($file);
    $file = reset($file);
    $this->assertInstanceOf(File::class, $file);
    $file_download_url = \Drupal::service('file_url_generator')->generateAbsoluteString($file->getFileUri());
    $this->assertNotNull($file_download_url);
    $this->drupalGet($file_download_url);
    $this->assertSession()->statusCodeEquals(200);
    $file_system = \Drupal::service('file_system');
    $file_scheme = 'temporary';
    $file_real_path = $file_system->realpath($file_scheme . '://' . explode('file=', parse_url($href, PHP_URL_QUERY))[1]);
    $zip_instance = \Drupal::service('single_content_sync.helper')->createZipInstance($file_real_path);
    $extract_dir = "{$file_scheme}://extract/zip";
    $zip_instance->extract($extract_dir);
    $files = $file_system->scanDirectory($extract_dir, '/' . str_replace('.', '\.', 'yml') . '$/');
    $this->assertNotEmpty($files);
    $file_uri = reset($files)->uri;
    $extracted_real_path = $file_system->realpath($file_uri);
    $content = Yaml::decode(file_get_contents($extracted_real_path));
    $this->assertIsArray($content);
    $this->assertSame($content['base_fields'], $this->exampleEntity['base_fields']);
  }

}
