<?php

namespace Drupal\Tests\flexible_descriptions_sync\Functional;

use Drupal\Core\Serialization\Yaml;
use Drupal\Tests\flexible_descriptions\Functional\FlexibleDescriptionsBrowserTestBase;
use Drupal\Tests\flexible_descriptions\Functional\FlexibleDescriptionsTestTrait;

/**
 * Provides base class for flexible descriptions sync tests.
 */
class FlexibleDescriptionsSyncBrowserTestBase extends FlexibleDescriptionsBrowserTestBase {

  use FlexibleDescriptionsTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'flexible_descriptions',
    'node',
    'flexible_descriptions_sync',
    'path',
    'single_content_sync',
  ];

  /**
   * {@inheritdoc}
   */
  protected $strictConfigSchema = FALSE;

  /**
   * Example entity.
   */
  protected array $exampleEntity;

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->createUser([
      'access content',
      'access administration pages',
      'create article content',
      'edit any article content',
      'delete any article content',
      'access content overview',
      'administer flexible_description',
      'export single content',
    ]);
    $this->drupalLogin($this->adminUser);
    $this->enableArticleDescriptions();

    // Prepare flexible description entity for test.
    $this->exampleEntity = Yaml::decode(file_get_contents(\Drupal::service('extension.list.module')->getPath('flexible_descriptions_sync') . '/tests/assets/flexible_description_example.yml'));
  }

}
