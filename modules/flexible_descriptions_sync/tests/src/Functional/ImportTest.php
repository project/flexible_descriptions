<?php

namespace Drupal\Tests\flexible_descriptions_sync\Functional;

use Drupal\Core\Serialization\Yaml;

/**
 * Provides tests of the flexible descriptions import functionality.
 *
 * @group flexible_descriptions_sync
 */
class ImportTest extends FlexibleDescriptionsSyncBrowserTestBase {

  /**
   * {@inheritDoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->createRole(['import single content'], 'importer');
    $this->adminUser->addRole('importer');
    $this->adminUser->save();
  }

  /**
   * Checks whether import functionality works as expected.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testFlexibleDescriptionsImport(): void {
    $this->drupalGet('admin/content/import');
    $this->assertSession()->statusCodeEquals(200);
    $this->assertSession()->elementExists('css', '[data-yaml-editor="true"]');
    $this->submitForm([
      'edit-content' => Yaml::encode($this->exampleEntity),
    ], 'Import');
    $this->assertSession()->pageTextNotContains('The content of the YAML file is not valid.');
    $this->assertSession()->pageTextContainsOnce('The content has been synced');
    $this->assertNotEmpty(\Drupal::entityTypeManager()->getStorage('flexible_description')->loadByProperties($this->exampleEntity['base_fields']));
  }

}
