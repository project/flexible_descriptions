<?php

namespace Drupal\Tests\flexible_descriptions\FunctionalJavascript;

/**
 * Tests description actions via HTMX.
 *
 * @group flexible_descriptions
 */
class HtmxActionsTest extends FunctionalJavascriptTestBase {

  /**
   * Checks add form behavior.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testAddAction(): void {
    // Go to node add page.
    $this->goToArticleAddForm();
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Open a form to set description.
    $page->pressButton('node|article|title');
    $assert_session->waitForElementVisible('css', '.flexible-description-htmx-form');

    // Set description text.
    $assert_session->waitForElementVisible('css', 'textarea[name="description-text"]');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    $page->fillField('description-text', 'New description added via htmx form!');
    $assert_session->waitForText('New description added via htmx form!');
    $page->pressButton('node|article|title');

    // Check if the description is saved & displayed.
    $assert_session->waitForElementVisible('css', '.flexible-description-text');
    $assert_session->waitForText('New description added via htmx form!');
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $this->assertTrue($result);
  }

  /**
   * Checks editing behavior.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testUpdateAction(): void {
    // Create description manually to avoid duplication of same steps.
    $result = $this->prepareArticleTitleFlexibleDescription();
    $this->assertEquals(SAVED_NEW, $result);

    // Go to article add page.
    $this->goToArticleAddForm();
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Open a form.
    $page->pressButton('node|article|title');
    $assert_session->waitForElementVisible('css', '.flexible-description-htmx-form');

    // Update description value.
    $assert_session->waitForElementVisible('css', 'textarea[name="description-text"]');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    $page->fillField('description-text', 'Updated description via htmx form!');
    $assert_session->waitForText('Updated description via htmx form!');
    $page->pressButton('node|article|title');

    // Check whether the value was updated.
    $assert_session->waitForElementVisible('css', '.flexible-description-text');
    $assert_session->waitForText('Updated description via htmx form!');
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $this->assertTrue($result);
  }

  /**
   * Test delete action.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  public function testDeleteAction(): void {
    // Prepare description.
    $result = $this->prepareArticleTitleFlexibleDescription();
    $this->assertEquals(SAVED_NEW, $result);

    // Go to article add form page.
    $this->goToArticleAddForm();
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();

    // Open a form.
    $page->pressButton('node|article|title');
    $assert_session->waitForElementVisible('css', '.flexible-description-htmx-form');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');

    // Clear textarea.
    $page->fillField('description-text', '');
    $page->pressButton('node|article|title');

    // Verify that it's empty now.
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $this->assertEmpty($assert_session->waitForElementVisible('css',
    // '.field--name-title .flexible-description-text', 20000)->getText());
  }

}
