<?php

namespace Drupal\Tests\flexible_descriptions\FunctionalJavascript;

use Drupal\FunctionalJavascriptTests\WebDriverTestBase;
use Drupal\user\Entity\User;

/**
 * Base class for functional javascript tests.
 */
abstract class FunctionalJavascriptTestBase extends WebDriverTestBase {

  use FunctionalJavascriptTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'flexible_descriptions',
    'flexible_descriptions_test',
    'node',
  ];

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Flexible descriptions editor.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $flexibleDescriptionsEditor;

  /**
   * Admin user.
   *
   * @var \Drupal\user\Entity\User
   */
  protected User $adminUser;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function setUp(): void {
    parent::setUp();
    // Prepare a node type we want to test on.
    $this->createContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);
    // Prepare users and flexible descriptions configurations.
    // Prepare users.
    $this->flexibleDescriptionsEditor = $this->createUser([
      'access content',
      'edit any article content',
      'access administration pages',
      'create article content',
    ]);

    $this->adminUser = $this->createUser([
      'access content',
      'access administration pages',
      'create article content',
      'edit any article content',
      'delete any article content',
      'access content overview',
      'administer flexible_description',
    ]);
    $this->drupalLogin($this->adminUser);
    $this->enableArticleDescriptions();
    $this->grantManagementPermission($this->flexibleDescriptionsEditor, 'node', 'article');
    $this->drupalLogin($this->flexibleDescriptionsEditor);
  }

}
