<?php

namespace Drupal\Tests\flexible_descriptions\FunctionalJavascript;

use Drupal\Core\Session\AccountInterface;
use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;
use Drupal\user\Entity\Role;

/**
 * Contains useful and re-usable methods for functional js tests.
 */
trait FunctionalJavascriptTestTrait {

  /**
   * Goes to article add form.
   */
  public function goToArticleAddForm(): void {
    $this->drupalGet('node/add/article');
  }

  /**
   * Create flexible description for article title field.
   */
  public function prepareArticleTitleFlexibleDescription(): bool {
    $article_title_description = [
      'langcode' => 'en',
      'label' => 'Flexible description | node|article|title',
      'description_text' => 'New description added via htmx form!',
      'description_identifier' => 'node|article|title',
    ];
    return FlexibleDescriptions::create($article_title_description)->save();
  }

  /**
   * Grants management permission.
   */
  protected function grantManagementPermission(
    AccountInterface $user,
    string $entity_type_id,
    string $entity_bundle
  ): void {
    $this->createRole(['manage flexible descriptions'], "{$entity_bundle}_f_descriptions_editor");
    $user->addRole("{$entity_bundle}_f_descriptions_editor");
    $user->save();

    // Add a permission to manage flexible descriptions for articles.
    $this->grantPermissions(Role::load("{$entity_bundle}_f_descriptions_editor"), ["manage flexible descriptions in $entity_type_id|$entity_bundle"]);
  }

  /**
   * Enables article node type flexible descriptions.
   */
  protected function enableArticleDescriptions(): void {
    $current_path = parse_url($this->getUrl(), PHP_URL_PATH);
    if ($current_path !== '/admin/structure/flexible-description') {
      $this->drupalGet('admin/structure/flexible-description');
    }
    $this->submitForm([
      'node[is_enabled]' => 1,
      'node[enabled_bundles][article]' => 1,
    ], 'Save');
  }

}
