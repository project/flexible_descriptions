<?php

namespace Drupal\Tests\flexible_descriptions\FunctionalJavascript;

/**
 * Tests whether the library is installed.
 *
 * @group flexible_descriptions
 */
class LibraryTest extends FunctionalJavascriptTestBase {

  /**
   * Checks whether htmx library has the appropriate file attached on the page.
   */
  public function testLibrary(): void {
    // Go to the article add page and check whether the library is on place.
    $this->goToArticleAddForm();
    $page = $this->getSession()->getPage();
    // Check if library file is at place.
    $library_file = $page->find('css', 'script[src*="htmx.min.js"]');
    $this->assertNotEmpty($library_file, 'Library file is not found.');
  }

}
