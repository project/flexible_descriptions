<?php

namespace Drupal\Tests\flexible_descriptions\FunctionalJavascript;

use Drupal\flexible_descriptions\Entity\FlexibleDescriptions;
use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Tests translation actions via HTMX.
 *
 * @group flexible_descriptions
 */
class HtmxTranslationTest extends FunctionalJavascriptTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'language',
  ];

  /**
   * Test translate behavior.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testHtmxTranslation(): void {
    // Add at least 1 language to test translations.
    ConfigurableLanguage::createFromLangcode('uk')->save();

    $translation_manager = \Drupal::service('content_translation.manager');
    $translation_manager->setEnabled('node', 'article', TRUE);
    $translation_manager->setEnabled('flexible_description', 'flexible_description', TRUE);

    // Prepare the original description.
    $result = $this->prepareArticleTitleFlexibleDescription();
    $this->assertEquals(SAVED_NEW, $result);

    $this->drupalGet('uk/node/add/article');
    $assert_session = $this->assertSession();
    $page = $this->getSession()->getPage();
    // Verify that there is "Add button", not the "Edit".
    $assert_session->elementTextEquals('css', 'button[id="node|article|title"]', 'Add flexible description');

    // Open a form.
    $page->pressButton('node|article|title');
    $assert_session->waitForElementVisible('css', '.flexible-description-htmx-form');

    // Set translated description.
    $assert_session->waitForElementVisible('css', 'textarea[name="description-text"]');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    $page->fillField('description-text', 'Створений опис українською');
    $page->pressButton('node|article|title');

    // Verify it was saved and now displayed on the page.
    $assert_session->waitForElementVisible('css', '.field--name-title .flexible-description-text');
    $assert_session->waitForText('Створений опис українською');
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $this->assertTrue($translation_set);
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    // Open a form again and try to update it.
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $this->assertEquals('Edit description', $button->getText());
    $page->pressButton('node|article|title');

    $assert_session->waitForElementVisible('css', 'textarea[name="description-text"]');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    $page->fillField('description-text', 'Оновлений опис українською');
    $page->pressButton('node|article|title');

    // Verify it is displayed now.
    $assert_session->waitForElementVisible('css', '.field--name-title .flexible-description-text');
    $assert_session->waitForText('Оновлений опис українською');

    // Last steps: delete it, verify deletion and check if original version
    // is not affected.
    $page->pressButton('node|article|title');
    $assert_session->waitForElementVisible('css', '.flexible-description-htmx-form');

    // Clear text.
    $assert_session->waitForElementVisible('css', 'textarea[name="description-text"]');
    $assert_session->waitForElementVisible('css', 'button[id="node|article|title"]');
    $page->fillField('description-text', '');
    $page->pressButton('node|article|title');

    $assert_session->waitForElementVisible('css', '.field--name-title .flexible-description-text');
    // @todo investigate why this assertion fails when running within gitlab ci.
    // $elem = $page
    // ->find('css', '.field--name-title .flexible-description-text');
    // $this->assertEmpty($elem->getText());
    $description_id = \Drupal::service('flexible_descriptions.helper')->getExistingDescription('node|article|title');
    $this->assertNotEmpty($description_id);
    $description = FlexibleDescriptions::load(reset($description_id));
    $this->assertCount(1, $description->getTranslationLanguages());
  }

}
