<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

use Drupal\language\Entity\ConfigurableLanguage;

/**
 * Test translation functionality.
 *
 * @group flexible_descriptions
 */
class TranslationTest extends FlexibleDescriptionsBrowserTestBase {

  use FlexibleDescriptionsTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'content_translation',
    'language',
  ];

  /**
   * Translate to langcode.
   *
   * @var string
   */
  private string $translateToLangcode;

  /**
   * Original description text.
   *
   * @var string
   */
  private string $originalDescriptionText;

  /**
   * Translation description text.
   *
   * @var string
   */
  private string $translationDescriptionText;

  /**
   * {@inheritDoc}
   */
  public function setUp(): void {
    parent::setUp();
    $this->drupalCreateRole(['manage flexible descriptions'], 'manager_flexible_descriptions');
    $this->adminUser->addRole('manager_flexible_descriptions');
    $this->adminUser->save();
    $this->enableArticleDescriptions();
    $this->grantManagementPermission($this->adminUser, 'node', 'article');

    $this->translateToLangcode = 'uk';
    $this->originalDescriptionText = 'Title flexible description, english';
    $this->translationDescriptionText = 'Опис поля заголовок, українська';

    // Add at least 1 language to test translations.
    ConfigurableLanguage::createFromLangcode($this->translateToLangcode)->save();

    // Enable flexible descriptions translation, also do it for nodes
    // so that we can check if it applied on entity translation forms.
    $translation_manager = \Drupal::service('content_translation.manager');
    $translation_manager->setEnabled('node', 'article', TRUE);
    $translation_manager->setEnabled('flexible_description', 'flexible_description', TRUE);
  }

  /**
   * Add flexible description translations and check if applied correctly.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  public function testTranslation(): void {
    // Go to the management form and add english description for article title.
    $this->drupalGet($this->managementFormUrl);
    $assert_session = $this->assertSession();
    $assert_session->elementExists('xpath', "//textarea[@name = 'node[article][entity_bundle_fields_descriptions][title]']");
    $this->submitForm([
      'node[article][entity_bundle_fields_descriptions][title]' => $this->originalDescriptionText,
    ], 'Save');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContainsOnce('Processed successfully.');

    // Go to an uk form and add translation.
    $this->drupalGet($this->translateToLangcode . '/' . $this->managementFormUrl);
    $this->submitForm([
      'node[article][entity_bundle_fields_descriptions][title]' => $this->translationDescriptionText,
    ], 'Save');
    $assert_session->statusCodeEquals(200);
    $assert_session->pageTextContainsOnce('Processed successfully.');

    // Check if original and translated versions exist.
    $identifier = 'node|article|title';
    $storage = \Drupal::entityTypeManager()->getStorage('flexible_description');
    $original_query_result = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('description_identifier', $identifier)
      ->condition('langcode', 'en')
      ->condition('description_text', $this->originalDescriptionText)
      ->execute();
    $translation_query_result = $storage
      ->getQuery()
      ->accessCheck(FALSE)
      ->condition('description_identifier', $identifier)
      ->condition('langcode', $this->translateToLangcode)
      ->condition('description_text', $this->translationDescriptionText)
      ->execute();
    $this->assertNotEmpty($original_query_result);
    $this->assertNotEmpty($translation_query_result);
    $this->assertEquals($original_query_result, $translation_query_result);

    // Check if translated description was applied on an entity form.
    $this->drupalGet($this->translateToLangcode . '/' . $this->addArticleUrl);
    $assert_session->pageTextContainsOnce($this->translationDescriptionText);

  }

}
