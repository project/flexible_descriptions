<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

use Drupal\Core\Entity\Entity\EntityFormDisplay;
use Drupal\field\Entity\FieldConfig;
use Drupal\field\Entity\FieldStorageConfig;

/**
 * Test how flexible descriptions behave after the creation of it.
 *
 * @group flexible_descriptions
 */
class DisplayTest extends FlexibleDescriptionsBrowserTestBase {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'field',
    'text',
  ];

  /**
   * Test display behavior.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testFlexibleDescriptionsDisplay(): void {
    // Firstly - we need to create a default description of some text field.
    $text_field_storage = FieldStorageConfig::create([
      'field_name' => 'textfield_to_test',
      'entity_type' => 'node',
      'type' => 'text',
    ]);
    $text_field_storage->save();
    $text_field = FieldConfig::create([
      'field_name' => 'textfield_to_test',
      'entity_type' => 'node',
      'bundle' => 'article',
      'label' => 'Textfield to test',
      'required' => FALSE,
      'description' => 'Initial textfield field description',
    ]);
    $text_field->save();

    // Prepare boolean field with a checkbox widget.
    $boolean_field_storage = FieldStorageConfig::create([
      'field_name' => 'boolean_field_to_test',
      'entity_type' => 'node',
      'type' => 'boolean',
    ]);
    $boolean_field_storage->save();
    $boolean_field = FieldConfig::create([
      'field_name' => 'boolean_field_to_test',
      'entity_type' => 'node',
      'bundle' => 'article',
      'label' => 'Boolean field to test',
      'required' => FALSE,
      'description' => 'Initial boolean field description',
    ]);
    $boolean_field->save();

    // Prepare ER field.
    $entity_reference_field_storage = FieldStorageConfig::create([
      'field_name' => 'er_field_to_test',
      'entity_type' => 'node',
      'type' => 'entity_reference',
      'cardinality' => 3,
      'settings' => [
        'target_type' => 'node',
      ],
    ]);
    $entity_reference_field_storage->save();
    $entity_reference_field = FieldConfig::create([
      'field_name' => 'er_field_to_test',
      'entity_type' => 'node',
      'bundle' => 'article',
      'label' => 'Label',
      'description' => 'Initial er field description',
      'settings' => [
        'handler' => 'default',
        'handler_settings' => [
          'target_bundles' => [
            'page' => 'page',
          ],
          'auto_create' => FALSE,
        ],
      ],
    ]);
    $entity_reference_field->save();

    // Update form display config.
    EntityFormDisplay::load('node.article.default')
      ->setComponent('textfield_to_test', ['type' => 'text_textfield'])
      ->setComponent('boolean_field_to_test', ['type' => 'boolean_checkbox'])
      ->setComponent('er_field_to_test', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
        ],
      ])
      ->save();

    // Now create a flexible descriptions for created fields.
    $flexible_descriptions_storage = \Drupal::entityTypeManager()
      ->getStorage('flexible_description');
    $flexible_descriptions_storage
      ->create([
        'description_text' => 'New textfield description',
        'description_identifier' => 'node|article|textfield_to_test',
      ])->save();
    $flexible_descriptions_storage
      ->create([
        'description_text' => 'New boolean field description',
        'description_identifier' => 'node|article|boolean_field_to_test',
      ])->save();
    $flexible_descriptions_storage
      ->create([
        'description_text' => 'New er field description',
        'description_identifier' => 'node|article|er_field_to_test',
      ])->save();

    // We cant test it on the node add form.
    $this->drupalGet($this->addArticleUrl);
    $assert_session = $this->assertSession();
    $assert_session->pageTextContainsOnce('New textfield description');
    $assert_session->pageTextContainsOnce('New er field description');
    $assert_session->pageTextContainsOnce('New boolean field description');
  }

}
