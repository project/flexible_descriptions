<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

use Drupal\Core\Session\AccountInterface;
use Drupal\Tests\UiHelperTrait;
use Drupal\Tests\user\Traits\UserCreationTrait;
use Drupal\user\Entity\Role;

/**
 * Contains useful methods to test flexible descriptions.
 */
trait FlexibleDescriptionsTestTrait {

  use UserCreationTrait;
  use UiHelperTrait;

  /**
   * Grants an access to manage flexible descriptions.
   */
  protected function grantManagementPermission(
    AccountInterface $user,
    string $entity_type_id,
    string $entity_bundle
  ): void {
    $this->createRole(['manage flexible descriptions'], "{$entity_bundle}_f_descriptions_editor");
    $user->addRole("{$entity_bundle}_f_descriptions_editor");
    $user->save();

    // Add a permission to manage flexible descriptions for articles.
    $this->grantPermissions(Role::load("{$entity_bundle}_f_descriptions_editor"), ["manage flexible descriptions in $entity_type_id|$entity_bundle"]);
  }

  /**
   * Enables article node type flexible descriptions.
   */
  protected function enableArticleDescriptions(): void {
    $current_path = parse_url($this->getUrl(), PHP_URL_PATH);
    if ($current_path !== '/admin/structure/flexible-description') {
      $this->drupalGet('admin/structure/flexible-description');
    }
    $this->submitForm([
      'node[is_enabled]' => 1,
      'node[enabled_bundles][article]' => 1,
    ], 'Save');
  }

}
