<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\taxonomy\Traits\TaxonomyTestTrait;
use Drupal\user\UserInterface;

/**
 * Browser test base class for Flexible descriptions functional tests.
 */
abstract class FlexibleDescriptionsBrowserTestBase extends BrowserTestBase {

  use TaxonomyTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $profile = 'minimal';

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'flexible_descriptions',
    'node',
    'taxonomy',
    'path',
  ];

  /**
   * An admin user.
   */
  protected UserInterface $adminUser;

  /**
   * A flexible descriptions editor.
   */
  protected UserInterface $flexibleDescriptionsEditor;

  /**
   * Settings form page url.
   */
  protected string $settingsFormUrl;

  /**
   * Management form page url.
   */
  protected string $managementFormUrl;

  /**
   * Add article page url.
   */
  protected string $addArticleUrl;

  /**
   * {@inheritdoc}
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  protected function setUp(): void {
    parent::setUp();
    // Prepare a node type we want to test on.
    $this->createContentType([
      'type' => 'article',
      'name' => 'Article',
    ]);

    // Prepare another node type, so we can check permissions behavior.
    $this->createContentType([
      'type' => 'page',
      'name' => 'Basic page',
    ]);

    // Also, prepare a taxonomy term vocabulary so terms container will
    // be available on management form.
    $this->createVocabulary();

    // Prepare users.
    $this->flexibleDescriptionsEditor = $this->createUser([
      'access content',
      'edit any article content',
      'access administration pages',
    ]);

    $this->adminUser = $this->createUser([
      'access content',
      'access administration pages',
      'create article content',
      'edit any article content',
      'delete any article content',
      'access content overview',
      'administer flexible_description',
    ]);

    // Do not use Url::fromRoute and then toString() because it will cause
    // 404 errors in gitlab ci tests.
    $this->settingsFormUrl = 'admin/structure/flexible-description';
    $this->managementFormUrl = 'admin/structure/manage-flexible-descriptions';
    $this->addArticleUrl = 'node/add/article';

    // Login as admin before any test.
    $this->drupalLogin($this->adminUser);
  }

}
