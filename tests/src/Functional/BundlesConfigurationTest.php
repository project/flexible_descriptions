<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

use Drupal\user\Entity\Role;

/**
 * Test whether node bundles are reflected configuration and management page.
 *
 * @group flexible_descriptions
 */
class BundlesConfigurationTest extends FlexibleDescriptionsBrowserTestBase {

  use FlexibleDescriptionsTestTrait;

  /**
   * Test bundles appearing on module forms pages.
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   * @throws \Behat\Mink\Exception\ExpectationException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testBundlesConfiguration(): void {
    $this->drupalGet($this->settingsFormUrl);
    $assert_session = $this->assertSession();
    $assert_session->elementExists('xpath', "//input[@name = 'node[is_enabled]']");
    $assert_session->elementExists('xpath', "//input[@name = 'node[enabled_bundles][article]']");
    $assert_session->elementExists('xpath', "//input[@name = 'node[enabled_bundles][page]']");
    // Once checked - we can verify that the user with appropriate permission
    // can configure descriptions on the management form.
    // First step - submit a configuration form, selecting just article bundle.
    $this->enableArticleDescriptions();
    // Before granting management accesses to  the editor - check that this user
    // has no access to the page.
    $this->drupalLogout();
    $this->drupalLogin($this->flexibleDescriptionsEditor);
    $this->drupalGet($this->managementFormUrl);
    $assert_session->statusCodeEquals(403);

    // Now grant the management access but not the bundle access.
    $this->drupalCreateRole(['manage flexible descriptions'], 'article_f_descriptions_editor');
    $this->flexibleDescriptionsEditor->addRole('article_f_descriptions_editor');
    $this->flexibleDescriptionsEditor->save();
    $this->drupalGet($this->managementFormUrl);
    $assert_session->statusCodeEquals(200);
    $assert_session->elementNotExists('xpath', "//details[@id = 'edit-node']");

    // Add a permission to manage flexible descriptions for articles.
    $this->grantPermissions(Role::load('article_f_descriptions_editor'), ['manage flexible descriptions in node|article']);
    // Now go to the management form and check whether flexible descriptions
    // editor can see elements according to the granted permissions.
    // Also, verify that user has no access to bundles to which the user
    // does not have a permission.
    $this->drupalGet($this->managementFormUrl);
    $assert_session->elementExists('xpath', "//details[@id = 'edit-node']");
    $assert_session->elementExists('xpath', "//details[@id = 'edit-node-article']");
    $assert_session->elementNotExists('xpath', "//details[@id = 'edit-node-page']");
    $assert_session->elementNotExists('xpath', "//details[@id = 'edit-taxonomy-term']");
  }

}
