<?php

namespace Drupal\Tests\flexible_descriptions\Functional;

/**
 * Test how the flexible descriptions management form works.
 *
 * Here we want to check whether CRUD actions actually work.
 *
 * @group flexible_descriptions
 */
class ManagementFormTest extends FlexibleDescriptionsBrowserTestBase {

  use FlexibleDescriptionsTestTrait;

  /**
   * {@inheritDoc}
   *
   * @throws \Behat\Mink\Exception\ElementNotFoundException
   */
  protected function setUp(): void {
    parent::setUp();
    $this->enableArticleDescriptions();
    $this->grantManagementPermission($this->flexibleDescriptionsEditor, 'node', 'article');
    $this->drupalLogin($this->flexibleDescriptionsEditor);
    $this->drupalGet($this->managementFormUrl);

    // Check whether title field exists.
    $this->assertSession()->elementExists('xpath', "//textarea[@name = 'node[article][entity_bundle_fields_descriptions][title]']");
    $this->submitForm([
      'node[article][entity_bundle_fields_descriptions][title]' => 'Title flexible description',
    ], 'Save');
  }

  /**
   * Tests how creation of flexible descriptions works.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testFlexibleDescriptionCreate(): void {
    // Ensure we see the flexible description after save.
    $this->assertSession()->pageTextContainsOnce('Title flexible description');
  }

  /**
   * Test how update of flexible descriptions work.
   *
   * @throws \Behat\Mink\Exception\ResponseTextException
   */
  public function testFlexibleDescriptionsUpdate(): void {
    // Change title flexible description and check whether it is updated.
    $this->submitForm([
      'node[article][entity_bundle_fields_descriptions][title]' => 'Title flexible description changed',
    ], 'Save');
    $this->assertSession()->pageTextContainsOnce('Title flexible description changed');
  }

  /**
   * Test how delete of flexible descriptions work.
   *
   * @throws \Behat\Mink\Exception\ExpectationException
   */
  public function testFlexibleDescriptionsDelete(): void {
    // Remove title flexible description and check whether it is deleted.
    $this->submitForm([
      'node[article][entity_bundle_fields_descriptions][title]' => '',
    ], 'Save');
    $this->assertSession()->fieldValueEquals('node[article][entity_bundle_fields_descriptions][title]', '');
  }

}
