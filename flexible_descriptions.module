<?php

/**
 * @file
 * Flexible descriptions module functionality.
 */

use Drupal\Component\Utility\NestedArray;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RouteMatchInterface;

/**
 * Implements hook_help().
 */
function flexible_descriptions_help($route_name, RouteMatchInterface $route_match) {
  if ($route_name === 'entity.flexible_description.settings') {
    $output = t('Settings form for a flexible description entity type.');
    $output .= '</br>' . t('Here you can configure which entity types and which bundles should work with the flexible descriptions.');
    return $output;
  }
}

/**
 * Implements hook_field_widget_complete_form_alter().
 */
function flexible_descriptions_field_widget_complete_form_alter(&$field_widget_complete_form, FormStateInterface $form_state, $context): void {
  $field_definition = $context['items']->getFieldDefinition();
  $base_field = $field_definition instanceof BaseFieldDefinition;
  if ($base_field) {
    $definition_id = implode('.', [
      $field_definition->getTargetEntityTypeId(),
      // We cannot use getTargetBundle() here because base fields are not
      // bundle specific.
      $context['items']->getEntity()->bundle(),
      $field_definition->getName(),
    ]);
  }
  else {
    $definition_id = $field_definition->id();
  }
  if (!empty($definition_id)) {
    /** @var \Drupal\flexible_descriptions\FlexibleDescriptionsHelperInterface $description_helper */
    $description_helper = \Drupal::service('flexible_descriptions.helper');
    [$entity_type_id, $bundle_id, $field_name] = explode('.', $definition_id);
    if ($entity_type_id === 'flexible_description') {
      return;
    }
    $description_identifier = $description_helper
      ->generateDescriptionIdentifier($entity_type_id, $bundle_id, $field_name);
    $description_text = $description_helper->getFlexibleDescriptionText($description_identifier);
    // Attach a library that allows to edit descriptions inline.
    $field_widget_complete_form['#attached']['library'][] = 'flexible_descriptions/inline-edit';
    $parents_array = $description_helper->getFieldWidgetDescriptionParents($field_widget_complete_form);
    // Show default description if flexible is not found.
    if (empty($description_text)) {
      $description_text = NestedArray::getValue($field_widget_complete_form, $parents_array);
    }
    $description_id = $description_helper->getExistingDescription($description_identifier);
    $description_markup_prepared = $description_helper->getFlexibleDescriptionMarkup($description_text, $description_identifier, reset($description_id));

    // Multiple value field array structure differs and handled here.
    if (isset($field_widget_complete_form['widget']['#theme']) &&
      $field_widget_complete_form['widget']['#theme'] === 'field_multiple_value_form') {
      if ($field_widget_complete_form['widget']['#cardinality'] === -1) {
        // Handles multiple values fields created by users.
        $field_widget_complete_form['widget']['#description'] = $description_markup_prepared;
      }
      elseif ($base_field) {
        // Handles node base fields.
        NestedArray::setValue($field_widget_complete_form,
          [
            'widget',
            0,
            'value',
            '#description',
          ], $description_markup_prepared);
      }
      elseif (!empty($parents_array)) {
        NestedArray::setValue($field_widget_complete_form, $parents_array, $description_markup_prepared);
      }
    }
    elseif (!empty($field_widget_complete_form['widget']['value'])
      && array_key_exists('#description', $field_widget_complete_form['widget']['value'])) {
      // Handle fields that have '#description' key in a widget array,
      // but uses value set in the 'value' one.
      NestedArray::setValue($field_widget_complete_form,
        [
          'widget',
          'value',
          '#description',
        ], $description_markup_prepared);
    }
    elseif (!empty($parents_array)) {
      NestedArray::setValue($field_widget_complete_form, $parents_array, $description_markup_prepared);
    }
  }
}

/**
 * Implements hook_theme().
 */
function flexible_descriptions_theme($existing, $type, $theme, $path): array {
  return [
    'flexible_descriptions_htmx' => [
      'template' => 'flexible-descriptions-htmx',
      'variables' => [
        'description_identifier' => NULL,
        'description_text' => NULL,
        'description_id' => NULL,
        'current_langcode' => \Drupal::languageManager()->getCurrentLanguage()->getId(),
      ],
    ],
  ];
}
