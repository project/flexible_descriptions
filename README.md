# Flexible Descriptions

A simple and useful way to set all the necessary field descriptions at one place.


## Table of contents

With Flexible Descriptions, any of the fields descriptions could be set from a single form page.
User should select a list of supported entity types before proceed to manage flexible descriptions.


## Requirements

The main module requires no modules outside of Drupal core.

The submodule `Flexible descriptions sync` requires
[Single Content Sync](https://www.drupal.org/project/single_content_sync)


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

### How to select entity types and their bundles in order to make flexible descriptions work?

To enable flexible descriptions for entity types and their bundles - user should navigate to the settings form
via main menu `Structure -> Flexible descriptions` or go to `/admin/structure/flexible-description`.

In the settings form user will be able to choose needed entity types and bundles.

If entity type is selected but no bundles checked, all the bundles will be available in the descriptions management form.


### How to handle descriptions for newly created entity type and/or bundle?

To add a descriptions setting via the descriptions management form user should
enable it in the settings form.


### Permissions system

The main module automatically generates per-bundle permissions for the enabled entity types and their bundles.


### Where user can set descriptions for previously selected entity types/bundles?

Users with granted permission to manage descriptions should navigate to the descriptions management form via the
main menu `Structure -> Manage flexible descriptions` or go to `/admin/structure/manage-flexible-descriptions`.

## Export/import of descriptions

### Can users export/import created descriptions?

Yes! To use this possibility - just enable the `Flexible descriptions sync` submodule.


### How to export descriptions?

To export created descriptions user should navigate to the export form via the main menu
`Content -> Export flexible descriptions` or go to `/admin/content/export-flexible-descriptions`
and select options from the list.


### How to import descriptions?

To import descriptions user should navigate to the flexible descriptions content page via the main menu
`Content -> Flexible descriptions` (see `Import` tab) or go to `/admin/content/import`.


## Maintainers

- Yaroslav Kozak - [quadrexdev](https://www.drupal.org/u/quadrexdev)
- Dmytro Sluchevskyi - [dmytro-aragorn](https://www.drupal.org/u/dmytro-aragorn)
- Lionel Enkaoua - [heyyo](https://www.drupal.org/u/heyyo)


## Flexible descriptions module page

Check out the project's page and feel free to report any issue or commit to the existing ones.

https://www.drupal.org/project/flexible_descriptions
